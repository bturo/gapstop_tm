.. _inputs:

Inputs preparation
==================

All inputs necessary to run the template matching are already provided in the "inputs" folder and this notebook aims 
to show how some of them were created using `cryoCAT <https://github.com/turonova/cryocat>`_ package.

Angle list preparation
----------------------

The angle list can be generated directly using GAPSTOP™ command line interface as described 
`here <cli.html>`_. Note that there is currently a bug that prevents using symmetry in angle generations and thus for 
the microtubule example one should use cryoCAT to generate the angles with C13 symmetry.

We generate two angular lists - one with 5 degrees angular step and one with 10 degrees. Both with symmetry C13.

.. code-block:: python
    :caption: Angle list creation

    import numpy as np
    from cryocat import geom

    angles_10 = geom.generate_angles(360, 10, symmetry=13)
    np.savetxt("angles_10_c13.txt", angles_10, fmt='%.2f', delimiter=',')

    angles_5 = geom.generate_angles(360, 5, symmetry=13)
    np.savetxt("angles_5_c13.txt", angles_5, fmt='%.2f', delimiter=',')



Mask creation
-------------

The mask can be created in cryoCAT using  `cryomask module
<https://cryocat.readthedocs.io/latest/generated/cryocat.cryomask.html>`_.

.. code-block:: python
    :caption: Mask creation

    from cryocat import cryomask

    # Creates a cylindrical mask that corresponds to the mask_microtubule.em
    _ = cryomask.cylindrical_mask(
        mask_size=52,
        radius=20,
        height=30,
        center=None,
        gaussian=2,
        gaussian_outwards=False,
        angles=None,
        output_name="test_mask.em",
    )


Wedge list creation
-------------------

Wedge list can be created in cryoCAT using `wedgeutils module
<https://cryocat.readthedocs.io/latest/generated/cryocat.wedgeutils.html>`_.


.. code-block:: python
    :caption: Wedge list creation

    from cryocat import wedgeutils

    # Creates wedge list for single tomogram from mdoc file and CTF estimation file obtained from GCTF
    _ = wedgeutils.create_wedge_list_sg(
        tomo_id = 126, 
        tomo_dim = [4060, 5760,2000], 
        pixel_size = 2.1760, 
        tlt_file = "126.mrc.mdoc", 
        z_shift=0.0, 
        ctf_file="126_gctf.star", 
        ctf_file_type='gctf', 
        dose_file="126.mrc.mdoc", 
        voltage=300.0, 
        amp_contrast=0.07, 
        cs=2.7, 
        output_file="test_wedge.star", 
        drop_nan_columns=True
        )


Peak analysis
-------------

The peak analysis module from `cryoCAT <https://github.com/turonova/cryocat>`_ provides tools to analyze the template 
candidates for template matching in STOPAGAP or GAPSTOP™. The results can be used to set up the parameters 
for the template matching.

Here we only show how to run it for the microtubule example, for the full explanation of the template list file as well
as all the steps visit the tutorial on the peak analysis on the 
`cryoCAT tutorial page <https://cryocat.readthedocs.io/latest/tutorials/peak_analysis/peak_analysis.html>`_.

Note that the following analysis can take some time due to large number of angles in angles_5_c13.txt 
(more than 20 minutes on a personal computer).

**IMPORTANT**: some of the results are written directly to the template_list.csv and thus this file should be closed 
during the analysis otherwise the "Permission denied" error can occur on some systems.

.. code-block:: python
    :caption: Peak analysis

    from cryocat import pana
    
    # Setup the paths
    parent_folder_path = './'
    angle_list_path = './'
    template_list = './template_list.csv'
    wedge_path = './'                       # not directly used in this tutorial but has to be set

    # Set indices
    indices = [0, 1]

    # Run peak analysis
    # Creates _scores.em, _angles.em, _dist_all.em, _dist_normals.em, _dist_inplane.em, and .csv file with basic 
    # stats - the mask overlap is for the soft mask used in TM
    pana.run_analysis(template_list, indices, angle_list_path, wedge_path, parent_folder_path)

    # Analysis of distance maps
    # Analysis of distance maps - it will get the area around the scores peak and label it, counts voxels, solidity and 
    # bounding boxes for all three distance maps
    # Creates labeled dist maps: _dist_all_label.em, _dist_normals_label.em, _dist_inplane_label.em, 
    # _dist_all_label_open.em, _dist_normals_label_open.em, _dist_inplane_label_open.em
    pana.compute_dist_maps_voxels(template_list, indices, parent_folder_path)

    # Mask statistics
    # Compute basic statistics on tight masks
    pana.get_mask_stats(template_list, indices, parent_folder_path)

    # Peak line profiles and statistics
    # Creates id_5_peak_line_profiles.csv file with peak profiles in x,y,z
    pana.compute_center_peak_stats_and_profiles(template_list, indices, parent_folder_path)

    # Tight mask overlap
    pana.check_existing_tight_mask_values(template_list, indices, parent_folder_path, angle_list_path)

    # Angular histograms
    # Creates histogram of scores values and peak value dependency on different angles
    # Creates outputs in _gradual_angles_analysis.csv and '_gradual_angles_histograms.csv files in the output folders
    pana.run_angle_analysis(template_list, indices, wedge_path, parent_folder_path, write_output = True)

    # Summary PDF 
    # Create pdf summary - fully based on the csv file
    # Creates _summary.pdf
    pana.create_summary_pdf(template_list, indices, parent_folder_path)


Open the summary pdf files in id_0_results and id_1_results to get some statistics on the peak analysis with 10 and 5 
degrees angular increment. The most differences can be seen in the cone distance map (the second to last row) - the 
10 degrees results in 180.0 degrees rotation around the peak suggesting that this angular increment is too big. For 
that reason one should proceed with angles_5_c13.txt for template matching.