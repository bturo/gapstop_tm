# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import os

project = "gapstop"
copyright = "2024, gapstop authors"
author = "gapstop authors"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx_copybutton",
    "sphinxcontrib.programoutput",
    "sphinx_design",
]

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (amed 'sphinx.ext.*') or your custom
# ones.
# extensions = [
#     "sphinx.ext.autodoc",
#     "sphinx.ext.doctest",
#     "sphinx.ext.coverage",
#     "sphinx.ext.mathjax",
#     "sphinx.ext.autosummary",
#     "sphinx.ext.intersphinx",
#     "matplotlib.sphinxext.plot_directive",
#     "numpydoc",

#     "sphinx_issues",
#     "sphinx_design",
#     "sphinx.ext.todo",
#     "nbsphinx",

# ]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# The root document.
root_doc = "index"

# The master toctree document.
# master_doc = "index"

# The suffix of source filenames.
# source_suffix = [".rst", ".ipynb"]

# The encoding of source files.
# source_encoding = "utf-8"


# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "docstrings", "nextgen", "Thumbs.db", ".DS_Store"]

# The reST default role (used for this markup: `text`) to use for all documents.
default_role = "literal"

# Sphinx-issues configuration
# issues_github_path = ""

# Include the example source for plots in API docs
plot_include_source = True
plot_formats = [("png", 90)]
plot_html_show_formats = False
plot_html_show_source_link = False
# plot_pre_code = """import numpy as np
# import pandas as pd"""

# nbsphinx do not use requirejs (breaks bootstrap)
# nbsphinx_requirejs_path = ""

# https://sphinx-toggleprompt.readthedocs.io/en/stable/#offset
# toggleprompt_offset_right = 35

# Don't add a source link in the sidebar
html_show_sourcelink = False

# Control the appearance of type hints
# autodoc_typehints = "none"
# autodoc_typehints_format = "short"

# Allow shorthand references for main function interface
# rst_prolog = """
# .. currentmodule:: cryocat
# """
# nbsphinx_execute = "never"
# nbsphinx_allow_errors = True

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "pydata_sphinx_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named 'default.css' will overwrite the builtin 'default.css'.
html_static_path = ["_static", "example_thumbs"]
for path in html_static_path:
    if not os.path.exists(path):
        os.makedirs(path)

html_css_files = ["css/gapstop.css"]

html_logo = "_static/gapstop_logo_wide_light_bcg.png"
html_favicon = "_static/favicon.png"

html_theme_options = {
    "sidebarwidth": 100,
    "icon_links": [
        {
            "name": "GitLab",
            "url": "https://gitlab.mpcdf.mpg.de/bturo/gapstop_tm",
            "icon": "fab fa-gitlab",
            "type": "fontawesome",
        },
    ],
    "show_prev_next": False,
    "navbar_align": "content",
    "navbar_start": ["navbar-logo"],
    "navbar_center": ["navbar-nav"],
    # "navbar_end": ["version-switcher", "theme-switcher", "navbar-icon-links"],
    "navbar_end": ["searchbox.html", "theme-switcher", "navbar-icon-links.html"],
    "header_links_before_dropdown": 8,
    "navbar_persistent": [],
    # "secondary_sidebar_items": ["class-page-toc"],
    "logo": {
        "image_light": "_static/gapstop_logo_wide_light_bcg.png",
        "image_dark": "_static/gapstop_logo_wide_dark_bcg.png",
    },
    # "switcher": {
    #    "version_match": "2.1",
    #    "json_url": "_static/versions.json",
    # },
}

html_context = {
    "default_mode": "light",
}

html_sidebars = {
    "index": [],
    "installation": [],
    "search": [],
    "**": ["sidebar_no_caption.html"],
}

toc_object_entries_show_parents = "hide"
numpydoc_show_inherited_class_members = False
