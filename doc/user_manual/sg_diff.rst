.. _sg_diff:

Differences to STOPGAP
======================

The overal algorithm approch within GAPStop se it the same as in STOPGAP and so are most of the inputs and outputs. 
However, there are few differences that users used to STOPGAP should be aware of:


Format
------
- STOPGAP: masks and templates have to be of the same format (.em, .mrc) and the format has to be specified as vol_ext 
  parameter. The output files have .em format.
- GAPSTOP™: the format of the masks and templates is evaluated on the fly based on their extension (.em, .mrc) allowing 
  for more flexibility (e.g., mask can be an em file, template mrc file). The parameter vol_ext influences the type of the 
  output that can either be .mrc or .em.

Folder structure
----------------
- STOPGAP: the required folder structure is quite complex and all folders have to be created before the run.
- GAPSTOP™: the only required folder is the one for outputs (specified by output_folder parameter) and if it does not
  exist it will be created. The inputs can be anywhere as long as path to them is correctly specified in the parameter file.

Angles numbering
----------------
- STOPGAP: The angles.em file holds indices corresponding to the rows in the angle_list files. Since STOPGAP is 
  implemented in MATLAB, which start indexing with 1, the range of values in angles.em files is [1, number_of_lines].
- GAPSTOP™: Since Python starts indexing with 0, the angles.em files can have values [0, number_of_lines-1]. 
  If a user wants to use STOPGAP functions, e.g. for peak extractions, the angles.em has to be changed by adding +1 to all 
  the values.

Euler angles
------------
- STOPGAP: The list with angles is a txt/csv file with three values on each line - each line encodes one rotation to
  probe. The rotations are specified with Euler angles with zxz convention, where z1 is phi angle, x is theta angle, 
  and z2 is psi angle. Phi defines in-plane rotation, while theta and psi defines a cone (out-of-plane) rotation. All 
  angles are in degrees. For historical reasons in STOPGAP files, the angles are stored in zzx order and only internally 
  they are used correctly az zxz. Thus the columns of the angle_list file are phi, psi, theta.
- GAPSTOP™: It is unitnuitive to have the angles stored in a different order that they are used, thus the default order 
  in angles_files in GAPStop is zxz (phi, theta, psi). To ensure compatibility, a user can use STOPGAP angles_list but 
  has to specify in a parameter angles_order that the order should be zzx (the default is zxz). In case the angles are 
  created on the fly and the angles_list is written out from GAPStop the angles_order parameter will also specify how 
  this file should be written out.

Symmetry
--------
- STOPGAP: It offers support for different symmetries (C, D, icosahedral...).
- GAPSTOP™: Currently, only support for C symmetry is implemented.