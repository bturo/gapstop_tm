.. _tm_params:

Parameters
==========

rootdir
  - Path to the working directory.
  - Type: string
  - Example: /path/to/working/dir/ or ./working_dir/ or working_dir/
outputdir
  - Path to the directory where the outputs should be stored. If the specified directory does not exist, it will be created.
  - Type: string
  - Optional parameter - default value is outputs/
vol_ext
  - Type of the format in which the resuls will be written out. The supported formats are EM and MRC.
  - Type: string
  - Allowed values: ".em" or ".mrc"
  - Optional parameter - default is ".em"
tomo_name
  - Tomogram name (including the path in case the file is not in the rootdir).
  - Type: string
  - Example: tomo_126.rec or /path/to/tomo/tomo_126.rec
tomo_num
  - Identification number of a tomogram. This number will be used to search for the correct tomogram information 
    in the provided wedge list.
  - Type: integer
  - Example: 126
tomo_mask_name
  - Path to the file with tomogram mask. The binary mask has to have the same dimensions as the tomogram. It is used in 
    two ways. First, the bounding box is computed to contain all values equal to 1. This can reduce computation time since
    the bounding box can be way smaller than the tomogram. The second time, the binary mask is used, is by outputing
    scores map where the mask is used to multiply the scores map so only values corresponding to the mask regions with 
    value 1 are kept. For the latter, same effect can be achieved by multiplying the scores map after the TM run. 
  - Type: string
  - Example: tomo_mask_126.rec or /path/to/tomo/mask/tomo_mask_126.rec
  - Optional parameter
wedgelist_name
  - Name of the wedge list file (including the path in case the file is not in the rootdir).
  - Type: string
  - Example: wedge_list.star or /path/to/wedge/list/wedge_list.rec
tmpl_name
  - Name of the template file. It can be in MRC or EM format. The constrast of the template has to correspond to the
    tomogram. For more information see `Required inputs <req_inputs.html#template>`__.
  - Type: string
  - Example: template.em or template.mrc or /path/to/the/template.mrc
mask_name
  - Name of the mask. It can be in MRC or EM format. For more information see `Required inputs <req_inputs.html#mask>`__.
  - Type: string
  - Example: template.em or template.mrc or /path/to/the/template.mrc
symmetry
  - Symmetry to be applied on the template prior the TM. Currently only cyclic symmetry is supported.
  - Type: string
  - Examples: C1 or C6 (the parameter is not case sensitive so c6 would work as well)
  - Optional parameter - default value is C1 (no symmetry)
anglist_name
  - Name of the file with Euler angles specifying the rotations. For more information see 
    `Required inputs <req_inputs.html#angle-list>`__.
  - Type: string
  - Examples: angles_5.txt or /path/to/the/angles_5.txt
  - Optional parameter - if not specified at least angincr has to be set in the parameter file (see below).
angincr
  - Out-of-plane angular increment in degrees. Defines how dense the cone will be sampled. In "zxz" Euler convention 
    influences the theta (x) and psi (z2) angles.
  - Type: float
  - Examples: 10 (for 10 degrees sampling) or 20.5 (for 20.5 degrees sampling)
  - Optional parameter - if not specified the anglist_name has to be set in the parameter file (see above).
angiter
  - Out-of-plane angular iterations in degrees. Defines the total cone angle to be sampled as angincr*angiter 
    (the maximum should be 180 degrees for the full sphere). In "zxz" Euler convention influences the theta (x) and 
    psi (z2) angles. 
  - Type: integer
  - Examples: 5 (with angincr of 10 it will sample the cone angle of 100 degrees)   
  - Optional - if not specified, it will be set so the full sphere is sampled.
  - Used only if anglist_name is not specified.
phi_angincr
  - In-plane angular increments in degrees. Defines how densely the in-plane rotation will be sampled. In "zxz" Euler 
    convention influences the phi (z1) angle. 
  - Type: float
  - Examples: 10 (for 10 degrees sampling) or 20.5 (for 20.5 degrees sampling)
  - Optional parameter - if not specified it will be set to the same value as angincr parameter.
  - Used only if anglist_name is not specified.
phi_angiter
  - In-plane angular iterations. Defines the total in-plane angle to be sampled as phi_angincr*phi_angiter (the 
    maximum should be 180 degrees for the full circle). In "zxz" Euler convention influences the phi (z1) angle. 
  - Type: integer
  - Examples: 5 (with phi_angincr of 10 it will sample the in-plane angle of 100 degrees)   
  - Optional - if not specified, it will be set so the circle is sampled.
  - Used only if anglist_name is not specified.
anglist_order
  - Specifies the order in which the input angles are specified. For more information see 
    `Required inputs <req_inputs.html#angle-list>`__ and `Differences to STOPGAP <sg_diff.html#euler-angles>`__
  - Note that this parameter influences also the order in which the angle list is written out if it is generated
    on-the-fly during the TM.
  - Type: string
  - Allowed values: "zxz" or "zzx"
  - Optional parameter - default is "zxz"
smap_name
  - Prefix of the name used to store scores map. The final name will be created as {prefix}_{template_num}_{tomo_number}.em.
    For example, if the value is set to "scores" and there is only one line in the parameter file (i.e., one template 
    and one tomogram, which has tomo_num 126) the final scores map will be stored in scores_0_126.em. If there are
    two lines of parameters, with the same tomo_num but different templates, the final scores maps will be stored in
    scores_0_126.em and scores_1_126.em.
  - Type: string
  - Examples: scores
  - Optional parameter - default is "scores"
omap_name
  - Prefix of the name used to store angles map. The final name will be created as {prefix}_{template_num}_{tomo_number}.em.
    in similar way as for smap_name (see above). Note that the indices in angles maps are numbered from 0, see
    `Differences to STOPGAP <sg_diff.html#angles-numbering>`__ for more information.
  - Type: string
  - Examples: angles
  - Optional parameter - default is "angles"
lp_rad
  - Low-pass filter radius in Fourier pixels/voxels to be applied to the template. To compute this value from the desired
    resolution, use following formula: round(template_box_size * pixel_size / resolution) where template_box_size
    is one dimension of the template.
  - Type: float
  - Examples: 20 (or 20.0)
lp_sigma
  - Sigma cut-off for the low-pass filter.
  - Type: float
  - Examples: 2.0 or 3.0
  - Optional parameter - default is 3.0
hp_rad
  - High-pass filter radius in Fourier pixels/voxels to be applied to the template. To compute this value from the desired
    resolution, use following formula: round(template_box_size * pixel_size / resolution) where template_box_size
    is one dimension of the template. Note that for most of the cases the optimal value is 1 (i.e. no high-pass 
    filter).
  - Type: float
  - Examples: 2 (or 2.0)
hp_sigma
  - Sigma cut-off for the high-pass filter.
  - Type: float
  - Examples: 2.0 or 3.0
  - Optional parameter - default is 2.0
binning
  - Binning of the tomogram. This value is used to adapt the values in the wedge-list (which are unbinned) to the correct
    binning. Note that binning of 1 corresponds to unbinned data (i.e. without any binning). Value of 2 corresponds to 
    the data that were downsampled by factor 2, value of 3 corresponds to the data that were downsampled by factor 3.
  - Type: integer
  - Examples: 1 or 2 or 3 or 4
  - We have noticed that sometimes better results are achieved by setting this parameter to 1 regardless of actual binning.
calc_exp
  - Whether to apply exposure-filtering to the wedge mask.
  - Type: boolean
  - Optional parameter - default is True
calc_ctf
  - Whether to apply CTF-filtering to the wedge mask.
  - Type: boolean
  - Optional parameter - default is True
apply_laplacian
  - Whether to apply laplacian transform to the volumes prior TM.
  - Type: boolean
  - Optional parameter - default is False
noise_corr
  - How many rounds of noise randomization should be run. If no randomization is wanted, the parameter should be set to 0.
  - Type: integer
  - Optional parameter - default is 1
write_raw
  - Whether to write out the scores maps also without the noise randomization filtering. This parameter is used only
    if noise_corr is set to values higher than 0.
  - Type: boolean
  - Optional parameter - default is False
tiling
  - Type of tomogram splitting during parallelization.
  - Type: string
  - Allowed values: "legacy" (replicating the original matlab source), "legacy_fix" (fixing a bug in the legacy tiling) 
    or "new" (optimal tiling)
  - Optional parameter - default is currently "legacy_fix" but will be changed to "new"


**IMPORTANT**:  You have to either use `anglist_name` or the angles parameters (`angincr`, 
`angiter`, `phi_angincr`, `phi_angiter`). If you keep both options in the parameter file the TM will run however it 
will produce outputs with zero values  ("black" volumes).

Path specification
------------------
Each folder or file name can be specified either by a relative or an absolute path. All relative paths are evaluated
w.r.t. the `rootdir` parameter. For example, if `rootdir` is "/path/to/the/rootdir/" and a template is specified
as "inputs/microtubule.em" (or "./inputs/microtubule.em") then the full path to the template will
be "/path/to/the/rootdir/inputs/microtubule.em". IF the template is "microtubule.em" then the full path will be
"/path/to/the/rootdir/microtubule.em". In case the `rootdir` is a relative path then the full path will be assembled
w.r.t. to the folder from which you run `gapstop` command or, in case you run it on cluster and specify a working
directory there, then the full path will be w.r.t. the working directory. For example, if the `rootdir` is 
"./tm_tutorial/" and you run `gapstop` command from the folder "/path/to/my/current/folder/" the full path will be
"/path/to/my/current/folder/tm_tutorial/" for the rootdir and all other relative paths in the parameter list will be
assembled w.r.t. this path.
