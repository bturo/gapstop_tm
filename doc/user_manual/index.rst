.. _usage:

.. toctree::
   :maxdepth: 2
   :hidden:

   Required inputs <req_inputs>
   TM parameters <tm_params>
   Command line interface <cli>
   Differences to STOPGAP <sg_diff>


User manual
===========


`Required inputs <req_inputs.html>`_
------------------------------------
The section describes the inputs that are required to run GAPSTOP™.


`TM parameters <tm_params.html>`_
---------------------------------
Detailed description of parameters that need to be set for the template matching.


`Command line interface <cli.html>`_
------------------------------------
Basic information on the command line interface.


`Differences to STOPGAP <sg_diff.html>`_
----------------------------------------
The section described the differences between STOPGAP and GAPSTOP™.