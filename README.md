# Gapstop(TM)

GPU Accelerated Python Stopgap for Template Matching

**gapstop** is able to leverage the power of GPU accelerated multi-node HPC
systems to be efficiently used for template matching. It speeds up template
matching by using an MPI-parallel layout and offloading the compute-heavy
correlation kernel to one or more accelerator devices per MPI-process
using [JAX](https://github.com/google/jax).

## Installation

Gapstop can be installled with pip:
```bash
pip install "gapstop @ git+https://gitlab.mpcdf.mpg.de/bturo/gapstop_tm.git"
```

### Dependencies

Gapstop requires the following dependencies:

* MPI. In case a working MPI implementation is available during installation,
  `mpi4py` will be installed automatically. It might be necessary to set the
  environment variable `MPICC` to point to the `mpicc` compiler wrapper. Please
  refer to the [official documentation](
  https://mpi4py.readthedocs.io/en/stable/install.html) for details.

* `jax` and `jaxlib`. A CPU-only version of gapstop can be readily
  installed by specifying the optional `[cpu]` dependency, e.g.:
  ```bash
  pip install "gapstop[cpu] @ git+https://gitlab.mpcdf.mpg.de/bturo/gapstop_tm.git"
  ```
  To use a GPU enabled version, please refer to the
  [jax documentation ](https://jax.readthedocs.io/en/latest) for
  information on the installation of jaxlib with GPU support on your system.
  ```

## Usage

See `gapstop --help` for a short help message.

Run template matching:

```bash
gapstop run_tm [-n <num_tiles>] [--random-seed <random_seed>] param_name
```

For a more detailed introduction please have a look at the
[documentation](https://bturo.pages.mpcdf.de/gapstop_tm/index.html).
