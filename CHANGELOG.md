# Changelog

## [0.3]

### Added

- possibility to restart unfinished runs.

### Changed

- move from precomputed distribution of tiles onto mpi-ranks to a file based
  tile queue. It simplifies restart functionality and will allow a more flexible
  choice of the number of ranks and will simplify running gapstop on non-
  homogeneous systems.
- log files for the individual tasks given in the input parameters are now written
  line-buffered, thus flushed more frequently giving a little more feedback
  than the (potentially buffered) stdio.

### Fixed

- reintroduced the mpi4py.util.pkl5.Intracomm wrapper that has been accidentally
  removed by b3d2fb2a243c6f6adadb73d57717ca93f0b90832>
- corrected package name and version metadata.

## [0.2]

### Added

- CHANGELOG.md.
- test case for the batch_params functionality.

### Fixed

- use of deprecated pandas api in batch_params.

### Changed

- move temporary storage from parallel hdf5 to simplified tile storage
  removing the dependency on h5py (and the parallel-io feature).
- interpret `noise-corr` parameter as boolean. This removes the capability
  to have more than 1 noise-corr iteration but can result in significant
  speedup.

## [0.1]

initial release.
