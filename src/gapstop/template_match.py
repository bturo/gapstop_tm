"""Stopgap template matching.

Driver module for template matching, which can be started with
func:`template_match`.
"""
import pathlib
import pickle
import time
import os
import io
import traceback
import warnings

import numpy as np
from mpi4py import MPI
from mpi4py.util import pkl5
try:
    import jax
except ModuleNotFoundError as e:
    msg = (
        "\ngapstop requires jax; please make sure jax is available."\
        "\nFor a cpu-only version, please run:"\
        "\n  pip install gapstop[cpu]"
        "\nFor a version supported by cuda12, please run:"
        "\n  pip install gapstop[cuda12]"
    )
    raise ImportError(msg) from e
import mrcfile
import emfile

from .stopgap_parallel_tm import stopgap_parallel_tm
from .setup import setup
from .util import tomo_size_from_mrc, mmap_volume, init_volume
from .config import read_params
from .log import logrotate
from .storage import TileStore, TileQueue
from .dd import TileIdx
from . import __version__

def _write_maps_cryo_format(
    idx,
    params,
    tilestore,
    tidx,
    voxel_size=1.0,
    rmtmp=False,
    ):
    """Transform output to em or mrc format."""

    print("\n-- Writing cryo-maps ...")

    # static parameters
    outdir = pathlib.Path(params["outputdir"])
    ext    = params["vol_ext"]

    # prepare output names
    fname  = "{}" + "_{}_{}{}".format(idx, params["tomo_num"], ext)
    s_name = outdir.joinpath(fname.format(params["smap_name"]))
    o_name = outdir.joinpath(fname.format(params["omap_name"]))
    n_name = outdir.joinpath(fname.format("noise_" + params["smap_name"]))
    r_name = outdir.joinpath(fname.format("raw_" + params["smap_name"]))

    # write scores and angles
    tomo_size = tuple(tomo_size_from_mrc(params["tomo_name"]))
    init_volume(s_name, shape=tomo_size, voxel_size=voxel_size, overwrite=True)
    init_volume(o_name, shape=tomo_size, voxel_size=voxel_size, overwrite=True)
    for t in range(tilestore.size()):
        bidx = tidx["box"][t]

        scores = tilestore["scores"][t]
        angles = tilestore["angles"][t]
        mask_name = params.get("tomo_mask_name")
        if not mask_name is None and not mask_name == "none":
            with mmap_volume(mask_name) as mask:
                _mask = mask[bidx].astype(np.float32)
                scores *= _mask
                angles += 1
                angles *= _mask.astype(np.int32)
                angles -= 1

        with mmap_volume(s_name) as out:
            out[bidx] = scores
        with mmap_volume(o_name) as out:
            out[bidx] = angles
        
    print(f"  {'/'.join(s_name.parts[-2:])}")
    print(f"  {'/'.join(o_name.parts[-2:])}")

    # write raw scores and noise-map
    if params["write_raw"] and params["noise_corr"]:
        init_volume(
            n_name,
            shape=tomo_size,
            voxel_size=voxel_size,
            overwrite=True,
        )
        init_volume(
            r_name,
            shape=tomo_size,
            voxel_size=voxel_size,
            overwrite=True,
        )
        for t in range(tilestore.size()):
            bidx  = tidx["box"][t]
            noise = tilestore["noise"][t]
            raw   = tilestore["scores_raw"][t]

            with mmap_volume(r_name) as out:
                out[bidx] = raw
            with mmap_volume(n_name) as out:
                out[bidx] = noise
            
        print(f"  {'/'.join(n_name.parts[-2:])}")
        print(f"  {'/'.join(r_name.parts[-2:])}")

    if rmtmp:
        print("\n  Cleaning up temporary output")
        tilestore.remove()
        os.remove(outdir / f"queue_{idx}.lock")

def _run_tm(
    idx,
    params,
    n_tiles,
    random_seed, 
    comm=MPI.COMM_WORLD,
    restart=False,
    cleanup=True,
    ):
    """'single-line' template matching.

    Parameters
    ----------
    idx:    int
        index for this run
    params: pd.Series or dict-like
        parameters for a single run of template matching
    n_tiles: int
        Number of tiles the tomogram is decomposed into.
    random_seed: int or None
        random seed used for deterministic simulation setup during testing.
    restart: bool
        whether to restart from previous queue.
    """

    rank       = comm.Get_rank()
    n_ranks    = comm.Get_size()
    batch_size = jax.device_count()

    outdir = pathlib.Path(params["outputdir"])

    # init-output
    if rank == 0:
        try:
            logname = pathlib.Path(
                params["outputdir"],
                f"{idx}.log"
            )
            if not outdir.exists():
                outdir.mkdir()
            logrotate(logname)
            errc = 0
        except Exception as e:
            errc = traceback.format_exc()
    else:
        errc = None
    errc = comm.bcast(errc, root=0)

    if not errc == 0:
        if rank == 0:
            msg = f"Initializing output failed for row {idx}. Skipping ..."
            raise RuntimeWarning(msg) from Exception(errc)
        return 1

    # setup-info
    if rank == 0:
        print(f"gapstop: template matching (version {__version__})")
        print("\n-- Backend:")
        print(f"tm is running on {jax.default_backend()}")
        print(f"tiles are batched over {batch_size} device(s) per rank")

        print("\n-- Parameters:")
        print(params.to_string())

    # setup parallel tile queue
    queuename = outdir / f"queue_{idx}"
    # check restart
    if restart and queuename.exists():
        tilequeue = TileQueue(queuename, n_tiles, comm, overwrite=False)
        state     = tilequeue.status()
        total     = state["n_tiles"]
        pending   = state["pending"]
        if pending == 0:
            if rank == 0:
                print("\nLine {idx} already complete. Skipping restart ...")
            return 0
        if rank == 0:
            msg = f"\nRestarting calculations for {pending} pending tiles "\
                  f"from {total} total tiles."
            print(msg)
            if (not n_tiles is None) and (not n_tiles == total):
                warnings.warn(
                    f"Restart is ignoring explicitly given '--ntiles = {n_tiles}'",
                    category=RuntimeWarning,
                )
        n_tiles = total
    else:
        #TODO: backup existing output to prevent accidental overwriting?
        n_tiles   = n_ranks * batch_size if n_tiles is None else n_tiles
        tilequeue = TileQueue(queuename, n_tiles, comm)

    # preprocessing
    if rank == 0:
        try:
            inp  = setup(params, n_tiles, random_seed)
            data = {"inp": inp, "errc": 0}
        except Exception as e:
            tb = traceback.format_exc()
            data = {"inp": None, "errc": tb}
    else:
        data = None
    data = comm.bcast(data, root=0)

    inp, errc = data.values()
    if not errc == 0:
        if rank == 0:
            msg = f"Setup failed for row {idx}. Skipping ..."
            raise RuntimeError(msg) from Exception(errc)
        return 1

    # wrap indices for tiles
    tidx = TileIdx(inp["c"])

    # tile storage
    tilestore     = TileStore(outdir / f"maps_{idx}")
    score_out     = tilestore.create_dataset('scores')
    score_raw_out = tilestore.create_dataset('scores_raw')
    noise_out     = tilestore.create_dataset('noise')
    angle_out     = tilestore.create_dataset('angles')

    # compute scores
    if rank == 0:
        tic = time.time()
        print("\n-- Computing scores...")

    for batch in tilequeue.process(batch_size):
        # dispatch score computation over batch
        scores, angles, noise, raw = stopgap_parallel_tm(
            params,
            inp["angles"],
            inp["tmpl"],
            inp["mask"],
            inp["tmpl_filt"],
            inp["tile_filt"],
            inp["c"],
            inp["tilesize"],
            inp["pr_tmpl"],
            batch,
        )

        # store results
        for i,t in enumerate(batch):
            cidx = tidx["crop"][t]

            score_out[t]     = scores[i][cidx]
            score_raw_out[t] = raw[i][cidx]
            angle_out[t]     = angles[i][cidx]
            noise_out[t]     = noise[i][cidx]

        if rank == 0:
            print(f"  processed {noise_out.size()}/{n_tiles} tiles")

    comm.Barrier()

    if rank == 0:
        if not tilestore.size() == n_tiles:
            warnings.warn(
                f"Processing of tiles is incomplete but {tilestore.size()}",
                category=RuntimeWarning,
            )
        print(f"\n Score computation took {time.time()-tic:.4g}s")

    if rank == 0:
        _write_maps_cryo_format(
            idx,
            params,
            tilestore,
            tidx,
            inp["pixelsize"],
            cleanup,
        )

def template_match(
    param_name,
    n_tiles=None,
    random_seed=None,
    restart=False,
    cleanup=True,
    ):
    """Run template matching.

    Starts template matching.

    Parameters
    ----------
    param_name: pathlib.Path or str
        path to the input parameters star-file.
    n_tiles: int
        Number of tiles the tomogram is decomposed into.
    random_seed: int or None
        random seed used for deterministic simulation setup during testing.
    restart: bool
        whether to restart to process unfinished tiles
    cleanup: bool
        cleanup temporary output files
    """

    # parallel setup
    comm = pkl5.Intracomm(MPI.COMM_WORLD)
    rank = comm.Get_rank()

    if rank == 0:
        try:
            params = read_params(param_name)
            data = {"params": params, "errc": 0}
        except Exception as e:
            tb = traceback.format_exc()
            data = {"params": None, "errc": tb}
    else:
        data = None
    data = comm.bcast(data, root=0)

    params, errc = data.values()
    if not errc == 0:
        if rank == 0:
            raise RuntimeError("Reading params failed. Aborting ...") \
                from Exception(errc)
        return 1

    for idx, line in params.iterrows():
        _run_tm(idx, line, n_tiles, random_seed, comm, restart, cleanup)
