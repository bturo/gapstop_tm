"""Modified exceprt from https://github.com/turonova/cryoCAT/blob/main/cryocat/geom.py"""
import re

import numpy as np
import pandas as pd
from scipy.spatial.transform import Rotation as srot

def normals_to_euler_angles(input_normals, output_order="zzx"):
    if isinstance(input_normals, pd.DataFrame):
        normals = input_normals.loc[:, ["x", "y", "z"]]
    elif isinstance(input_normals, np.ndarray):
        normals = input_normals
    else:
        raise ValueError("The input_normals have to be either pandas dataFrame or numpy array")

    theta = np.degrees(np.arctan2(np.sqrt(normals[:, 0] ** 2 + normals[:, 1] ** 2), normals[:, 2]))

    psi = 90 + np.degrees(np.arctan2(normals[:, 1], normals[:, 0]))
    b_idx = np.where(np.arctan2(normals[:, 1], normals[:, 0]) == 0)
    psi[b_idx] = 0

    phi = np.random.rand(normals.shape[0]) * 360

    if output_order == "zzx":
        angles = np.column_stack((phi, psi, theta))
    else:
        angles = np.column_stack((phi, theta, psi))

    return angles
    
def number_of_cone_rotations(cone_angle, cone_sampling):
    # Theta steps
    theta_max = cone_angle / 2
    temp_steps = theta_max / cone_sampling
    theta_array = np.linspace(0, theta_max, round(temp_steps) + 1)
    arc = 2.0 * np.pi * (cone_sampling / 360.0)

    number_of_rotations = 2  # starting and ending angle

    # Generate psi angles
    for i, theta in enumerate(theta_array[(theta_array > 0) & (theta_array < 180)]):
        radius = np.sin(theta * np.pi / 180.0)  # Radius of circle
        circ = 2.0 * np.pi * radius  # Circumference
        number_of_rotations += np.ceil(circ / arc) + 1  # Number of psi steps

    return number_of_rotations


def sample_cone(cone_angle, cone_sampling, center=np.array([0.0, 0.0, 0.0]), radius=1.0):
    # Creates "even" distribution on sphere. This is in many regards
    # just an approximation. However, it seems to work for not too extreme cases.
    # Source:
    # https://stackoverflow.com/questions/9600801/evenly-distributing-n-points-on-a-sphere/26127012#26127012

    number_of_points = number_of_cone_rotations(cone_angle, cone_sampling)

    # golden angle in radians
    phi = np.pi * (3 - np.sqrt(5))
    cone_size = cone_angle / 180.0

    sampled_points = [[0.0, 0.0, 1.0]]
    for i in np.arange(1, number_of_points, 1):
        # z goes from 1 to -1 for 360 degrees (i.e., a full sphere), is less
        z = 1 - (i / (number_of_points - 1)) * cone_size

        sp_radius = np.sqrt(1 - z * z)

        # golden angle increment
        theta = phi * i
        x = np.cos(theta) * sp_radius
        y = np.sin(theta) * sp_radius

        x = x * radius + center[0]
        y = y * radius + center[1]
        z = z * radius + center[2]

        sampled_points.append(np.array([x, y, z]))

    return np.stack(sampled_points, axis=0)


def generate_angles(
    angincr, #cone_sampling,
    angiter = None, #cone_angle = 360.0,   
    phi_angincr = None,  # inplane_sampling=None,
    phi_angiter = None,  # inplane_angle=360.0,
    symmetry="C1",       # or float/int
    zzx_order = False  # the correct one, the old one would be "zzx"
):
    
    if angiter is None:
        angiter = 180 / angincr

    # Calculate phi angles
    if phi_angincr is None:
        phi_angincr = angincr

    if phi_angiter is None:
        phi_angiter = 180 / phi_angincr

    if isinstance(symmetry,str):
        if "c" in symmetry.lower():
            symmetry = float(re.findall(r'\d+', symmetry)[0])
        else:
            raise ValueError("Only C symmetry is currently supported")

    cone_angle = angiter*angincr*2
    inplane_angle = angiter*angincr*2  # 360 -> full circle

    points = sample_cone(cone_angle, angincr)
    angles = normals_to_euler_angles(points, output_order="zxz")
    angles[:, 0] = 0.0

    starting_phi = 0.0

    # if case of no starting angles one can directly do cone_angles = angles
    # but going through rotation object will set the angles to the canonical set
    cone_rotations = srot.from_euler("zxz", angles=angles, degrees=True)
    cone_angles = cone_rotations.as_euler("zxz", degrees=True)
    cone_angles = cone_angles[:, 1:3]


    if inplane_angle != 360.0:
        phi_max = min(360.0 / symmetry, inplane_angle)
    else:
        phi_max = inplane_angle / symmetry

    phi_steps = phi_max / phi_angincr
    phi_array = np.linspace(0, phi_max, round(phi_steps) + 1)
    phi_array = phi_array[:-1]  # Final angle is redundant

    if phi_array.size == 0:
        phi_array = np.array([[0.0]])

    n_phi = np.size(phi_array)
    phi_array = phi_array + starting_phi

    # Generate angle list:
    # this now gives "zxz"  - phi, theta, psi
    angular_array = np.concatenate(
        [
            np.tile(phi_array[:, np.newaxis], (cone_angles.shape[0], 1)),
            np.repeat(cone_angles, n_phi, axis=0),
        ],
        axis=1,
    )
    if zzx_order:
        angular_array = angular_array [:,[0,2,1]] # phi, psi, theta

    return angular_array
