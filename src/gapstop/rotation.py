import jax.numpy as np
from jax.scipy.ndimage import map_coordinates
from jax.scipy.interpolate import RegularGridInterpolator

def _interpolate(arr, x, order=1, extrapolate='fill_zero'):
    """Handle extrapolation.

    map_coordinates cannot reproduce tom_rotate.c's way of 'extrapolation',
    so that why here is this temporary(?) hidden switch.
    """
    if extrapolate == 'fill_zero':
        iarr = map_coordinates(arr, x, order=order)
    elif extrapolate == 'none':
        m    = {1: "linear", 0: "nearest"}
        p    = tuple([np.arange(i) for i in arr.shape])
        ip   = RegularGridInterpolator(p, arr, fill_value=0, method=m[order])
        iarr = ip(x.T)
    else:
        raise ValueError(f"Unknown extrapolation method: {extrapolate}")

    return iarr.reshape(arr.shape)

def rot3(image, euler, center, extrapolate='fill_zero'):
    """Rotate image around 'center' by (zxz-extrinsic) euler-angles 'euler'.

    For the rotations, this function assumes the coordinate system to be
    mapped to the shape of 'image' as 'image.shape <-> (z,y,x). This is
    consistent with the mrc standard's default specification and also
    how mrcfile understands arrays.
    """

    #TODO: check for proper center.shape here.

    # some angle transformations
    euler = euler * np.pi / 180
    cos   = np.cos(euler)
    sin   = np.sin(euler)

    # rotation matrix (from extrinsic zxz angles)
    #from sympy import symbols
    #from sympy.physics.vector import ReferenceFrame
    #phi, theta, psi = symbols('phi, theta, psi')
    #B = ReferenceFrame('B')
    #N = ReferenceFrame('N')
    #B.orient_space_fixed(N, (phi, theta, psi), 'zxz')
    #N.dcm(B)
    phi = 0; theta = 1; psi = 2
    rm = np.array([
        [
         cos[psi]*cos[phi] - cos[theta]*sin[psi]*sin[phi],
         -cos[psi]*sin[phi] - cos[theta]*sin[psi]*cos[phi],
          sin[theta]*sin[psi]
        ],
        [
          sin[psi]*cos[phi] + cos[theta]*cos[psi]*sin[phi],
         -sin[psi]*sin[phi] + cos[theta]*cos[psi]*cos[phi],
         -sin[theta]*cos[psi]
        ],
        [
         sin[theta]*sin[phi],
         sin[theta]*cos[phi],
         cos[theta]
        ]
    ])

    # grid coordinates (with standard mrc coordinates definition)
    z,y,x = np.mgrid[0:image.shape[0],0:image.shape[1],0:image.shape[2]]
    grid  = np.array([x.ravel(), y.ravel(), z.ravel()])
  
    # apply the reverse rotation, so we can do the proper push-forward action
    # on the volume by interpolation below
    rgrid = np.dot(rm.T, grid-center[(2,1,0),np.newaxis])
    rgrid = rgrid[(2,1,0),:] + center[:,np.newaxis]

    return _interpolate(image, rgrid, order=1, extrapolate=extrapolate)

def rot2(image, alpha, center, extrapolate='fill_zero'):
    """Rotate 2d image around 'center'."""

    alpha = alpha * np.pi / 180
    cos   = np.cos(alpha)
    sin   = np.sin(alpha)

    rm = np.array([
        [ cos, -sin],
        [ sin,  cos]
    ])

    # grid coordinates
    y,x = np.mgrid[0:image.shape[0],0:image.shape[1]]
    grid  = np.array([x.ravel(), y.ravel()])

    # apply the reverse rotation, so we can do the proper push-forward action
    # on the volume by interpolation below
    rgrid = np.dot(rm.T, grid-center[(1,0),np.newaxis])
    rgrid = rgrid[(1,0),:] + center[:,np.newaxis]

    return _interpolate(image, rgrid, order=1, extrapolate=extrapolate)

def rotate_volume(vol, euler, center=None, extrapolate='fill_zero'):
    """Convenience wrapper around rot3 to mimick matlab interface.

    'extrapolate' allows two options:
      'fill_zero': enables smooth extrapolation by extending the interpolated
          image with zeros.
      'none': fills all pixels for which interpolation would require data
          outside the input image with zeros. This is how tom_rotate.c
          treats 'extrapolation'.
    """

    if center is None:
        center = np.floor(np.array(vol.shape) / 2)

    return rot3(vol, euler, center, extrapolate)

def rotate_image(img, alpha, center=None, extrapolate='fill_zero'):
    """Convenience wrapper around rot2 to mimick matlab interface."""

    if center is None:
        center = np.floor(np.array(img.shape) / 2)

    return rot2(img, alpha, center, extrapolate)
