"""Handle log output."""
import sys

# keep original stdout
stdout = sys.stdout

class _SplitLogger:
    def __init__(self, logname):
        self.stream  = stdout
        self.logfile = open(logname, "w")
        # flush more frequently on the logfile
        self.logfile.reconfigure(line_buffering=True)

    def write(self, data):
        """Write data to file and stream."""
        self.logfile.write(data)
        self.stream.write(data)

    def close(self):
        self.logfile.close()
        sys.stdout = stream

    def flush(self):
        self.logfile.flush()
        self.stream.flush()


def logrotate(logname):
    """Reset logfilename."""
    log = _SplitLogger(logname)
    sys.stdout = log
