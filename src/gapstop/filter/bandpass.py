"""Generate filters for tile and template."""
import warnings

import numpy as np
from scipy import signal, interpolate


def _frequencyarray(shape, pixelsize):
    """Magnitude of n-dim fftfreq."""
    freqs = np.array(
        np.meshgrid(
            *[np.fft.fftshift(np.fft.fftfreq(n,pixelsize)) for n in shape],
            indexing='ij',
        )
    )
    return np.sqrt(np.sum(freqs**2, axis=0))

def _bandpass_1d_legacy(r, lpr, lps, hpr, hps):
    """Almost Literal port of gsg_bandpass_filter_1d."""

    dist = np.arange(float(r)+1)

    lpf                   = np.ones_like(dist)
    lp_idx                = dist > lpr
    lpf[lp_idx]           = np.exp( -((dist[lp_idx] - lpr)/lps)**2 )
    lpf[lpf < np.exp(-2)] = 0

    hpf                   = np.ones_like(dist)
    hp_idx                = dist > hpr
    hpf[hp_idx]           = np.exp( -((dist[hp_idx] - hpr)/hps)**2 )
    hpf[hpf < np.exp(-2)] = 0

    return dist/r, lpf - hpf

def _bandpass_1d(taps, bands, scale, fs):
    """firwin bandpass with exponential window."""

    b = signal.firwin(
        taps,
        bands,
        window=('exponential', None, scale),
        pass_zero='bandpass',
        fs=fs,
    )
    w, h = signal.freqz(b, fs=fs)
    return w, h

def _generate_bpf_legacy(shapes, lpr, hpr, lps=None, hps=None):
    """Generate bandpass filter in nD by radial interpolation.

    In compatibility with matlab-stopgap this function provides
    circular interpolation of a 1d bandpass filter on differently
    shaped multidimenional arrays. Thereby, the frequency space of
    the 1d bandpass is set-up to coincide with np.fft.fftfreq(n, 0.5),
    with n being the size of the first dimension of the first shape
    in 'shapes'. To match gsg_generate_tm_bpf, the input shapes must
    be of length 2 with the first shape referring to the template.
    """  

    if not isinstance(shapes, list):
        shapes = [shapes]

    lps = 3 if lps is None else lps
    hps = 2 if hps is None else hps

    r    = np.ceil(shapes[0][0]/2)
    w, h = _bandpass_1d_legacy(r, lpr, lps, hpr, hps)

    filters = []
    for shp in shapes:
        freqs = _frequencyarray(shp, 0.5)
        bpf   = interpolate.pchip_interpolate(w, h, freqs)
        bpf[freqs > 1] = 0.
        # TODO: to have _frequencyarray consistent with gsg_frequencyarray
        # there is the fftshift in _frequencyarray. But this is actually not
        # necessary and can be removed once consistency at that level is not
        # needed anylonger; So as soon as this module is feature complete!
        filters.append(np.fft.ifftshift(bpf))
    return filters

def _generate_bpf(shapes, bands, scale, numtaps=10):
    """Generate bandpass filter in nD by radial interpolation."""

    warnings.warn("_generate_bpf is untested")
    
    if not isinstance(shapes, list):
        shapes = [shapes]

    w, h  = _bandpass_1d(numtaps, bands, scale, 2.0)

    filters = []
    for shp in shapes:
        freqs = _frequencyarray(shp, 0.5)
        bpf   = interpolate.pchip_interpolate(w, h, freqs)
        bpf[freqs > 1] = 0.
        # TODO: to have _frequencyarray consistent with gsg_frequencyarray
        # there is the ffthsift in _frequencyarray. But this is actually not
        # necessary and can be removed once consistency at that level is not
        # needed anylonger; So as soon as this module is feature comlete!
        filters.append(np.fft.ifftshift(bpf))

    return filters

def generate_bpf(shapes, params):
    """Interface to generate bandpass filter."""
    lpr = params["lp_rad"]
    hpr = params["hp_rad"]
    lps = params.get("lp_sigma")
    hps = params.get("hp_sigma")
    return _generate_bpf_legacy(shapes, lpr, hpr, lps, hps)
