"""Generate filters for template matching."""
from ._wedgemask import generate_wedgemask_slices
from ._exposure import generate_exposure
from ._ctf import generate_ctf


def generate_filters(tmpl_bpf, tile_bpf, wedgelist, params):
    """Generate filters for template matching."""

    calc_ctf      = params.get("calc_ctf")
    calc_exposure = params.get("calc_exp")

    # get relevant subset of the wedgelist
    wedgelist = wedgelist.loc[params["tomo_num"]]

    slice_idx, slice_weight, bin_wedge, tile_bin_wedge = \
        generate_wedgemask_slices(wedgelist, tmpl_bpf, tile_bpf)

    # init filters
    tmpl_filt = bin_wedge * tmpl_bpf
    tile_filt = tile_bin_wedge * tile_bpf

    # update template filter
    if calc_exposure:
        tmpl_filt *= generate_exposure(
            wedgelist,
            slice_idx,
            slice_weight,
            params
        )
        
    if calc_ctf:
        tmpl_filt *= generate_ctf(wedgelist, slice_idx, slice_weight, params)

    return tmpl_filt, tile_filt
