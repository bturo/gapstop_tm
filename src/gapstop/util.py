"""pygssg common utils."""
import pathlib
import re
import struct
import os
import contextlib

import mrcfile
import emfile
import starfile
import numpy as np
import pandas as pd

from . import cryocat_geom as geom

def read_volume(fname):
    """Read volume from cryo-files."""
    if isinstance(fname, str):
        fname = pathlib.Path(fname)

    ext = fname.suffix
    if ext == ".em":
        return emfile.read(fname)[1]
    elif ext in [".mrc", ".rec"]:
        return mrcfile.read(fname)
    else:
        raise ValueError("Unknown file format: {}".format(ext))

def write_volume(fname, vol, voxel_size=1):
    """Write volume as cryo file."""

    if isinstance(fname, str):
        fname = pathlib.Path(fname)

    ext = fname.suffix
    if ext == ".em":
        # voxelsize is ignored in the matlab code, so it is here
        emfile.write(fname, vol, overwrite=True)
    elif ext in [".mrc", ".rec"]:
        mrcfile.write(fname, vol, voxel_size=voxel_size, overwrite=True)
    else:
        raise ValueError("Unknown file format: {}".format(ext))

def init_volume(name, shape, voxel_size, overwrite=False):
    """Allocate float32 output files on disk."""

    name = pathlib.Path(name)
    if name.exists() and not overwrite:
        raise ValueError(f"file {name} exists")

    if name.suffix in (".mrc", ".rec"):
        mmap = mrcfile.new_mmap(name, shape, mrc_mode=2, overwrite=True)
        mmap.voxel_size = voxel_size
        mmap.close()
    elif name.suffix == ".em":
        # emfile doesn't readily provide mmap for creation therefore here
        # it gets a little clumsy
        hs = struct.Struct(''.join(emfile.specs.header_spec.values())) 
        # create emfile passing through memory with small mock data
        header_upd = {"xdim": shape[2], "ydim": shape[1], "zdim": shape[0]}
        data = np.array([[[0]]], dtype=np.float32)
        emfile.write(name, data, header_params=header_upd, overwrite=True)
        # increase file to full size
        new_size = hs.size + np.prod(shape) * 4 #float32
        os.truncate(name, new_size)
    else:
        raise ValueError(f"unsupported file extension {name.suffix}")

@contextlib.contextmanager
def mmap_volume(name):
    """Open existing output file with memory mapping for r/w."""

    name = pathlib.Path(name)

    if not name.exists():
        msg = f"file {name} not found. Use init_volume to create."
        raise FileNotFoundError(msg)

    if name.suffix in (".mrc", ".rec"):
        mmap = mrcfile.mmap(name, mode="r+")
        try:
            yield mmap.data
        finally:
            mmap.close()
    elif name.suffix == ".em":
        # as above: emfile doesn't readily provide mmap for r/w therefore here
        # it gets a little clumsy
        hs = struct.Struct(''.join(emfile.specs.header_spec.values())) 
        header, _ = emfile.read(name, header_only=True)
        dtype = emfile.specs.dtype_spec[header["dtype"]]
        shape = header['zdim'], header['ydim'], header['xdim']
        fp = open(name, "r+b")
        try:
            mmap = np.memmap(fp, dtype=dtype, shape=shape, offset=hs.size)
            yield mmap
        finally:
            fp.close()
    else:
        raise ValueError(f"unsupported file extension {name.suffix}")

def tomo_size_from_mrc(fname):
    """Get tomogram size from mrcfile."""
    with mrcfile.open(fname, header_only=True) as mrc:
        tomo_size = np.array(mrc.header[["nz", "ny", "nx"]].tolist())
    return tomo_size

def batch_params(tomo_list, template_name, outfile=None):
    """Create multiline input parameters from template."""
    nums = np.loadtxt(tomo_list, dtype=int)
    if not nums.ndim == 1:
        msg = "tomo_list has wrong number of dimensions. Must be linear."
        raise ValueError(msg)

    if len(nums) == 0:
        raise ValueError("tomo_list is empty.")

    template = starfile.read(template_name)

    if not len(template) == 1:
        msg = "Wrong number of lines in batch-template. Must be 1."
        raise ValueError(msg)

    # find token
    name  = template.loc[0, "tomo_name"]
    token = re.findall("\$[xX]+", name)

    if len(token) == 0:
        msg = "Impossible to identify token in tomo-name-template."
        raise ValueError(msg)

    utoken = np.unique(token)
    if not len(utoken) == 1:
        msg = "Token with different lengths found in template."
        raise ValueError(msg)

    tok = utoken[0]

    if len(tok) < len(str(max(nums))):
        msg = "Misspecified tomo-name-template. " \
              "Token is shorter than maximum tomo-num."
        raise ValueError(msg)

    out = pd.DataFrame()
    for i,tn in enumerate(nums):
        out  = pd.concat([out, template], ignore_index=True)
        fill = f"{tn:0>{len(tok)-1}d}"
        out.loc[i, ["tomo_name", "tomo_num"]] = (
            name.replace(tok, fill),
            tn
        )

    if outfile is None:
        outfile = pathlib.Path(template_name)
        outfile = outfile.with_suffix(f".batch{outfile.suffix}")
    starfile.write(out, outfile)

    print(f"Created multiline parameters: {outfile}")

    return outfile
