"""Stopgap tm command line interface.

Command line interface to run template matching. See `gapstop --help`.
"""

import argparse
import importlib

from . import __version__
from .util import batch_params
from .config import write_config, gen_angles

_descr = f"""Stopgap template matching. ({__version__})

This application can be used for stopgap template matching.
"""

def _tm(args):
    """Lazy-load tm only at tm-dispatch so cli can work without MPI."""
    tm = importlib.import_module(".template_match", "gapstop")
    tm.template_match(
        args.params,
        args.ntiles,
        args.random_seed,
        args.restart,
        args.cleanup,
    )

def _batch_params(args):
    """Create multiline params from tomo-batch."""
    batch_params(args.batch, args.template, args.output)

def _config(args):
    """Write parameter file with defauls."""
    write_config(args.output, args.no_defaults)

def _angles(args):
    """Generate angles."""
    gen_angles(
        args.angincr,
        args.angiter,
        args.phi_angincr,
        args.phi_angiter,
        args.symmetry,
        args.order
    )

def sg_tm():
    """Shell entrypoint to run template matching."""

    parser = argparse.ArgumentParser(
        description=_descr,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.set_defaults(func=lambda x: parser.print_usage())

    subparsers = parser.add_subparsers(
        title="subcommands",
        description=None,
    )

    # tm
    tm_descr = "Run template matching."
    run_tm = subparsers.add_parser(
        "run_tm",
        description=tm_descr,
        help="run template matching"
    )
    run_tm.add_argument(
        "params",
        help="path to input parameters star-file.",
        type=str,
    )
    run_tm.add_argument(
        "-n",
        "--ntiles",
        help="(optional) number of tiles to decompose tomogram.",
        type=int,
    )
    run_tm.add_argument(
        "--random-seed",
        help="(optional) random seed used for testing.",
        type=int,
    )
    run_tm.add_argument(
        "--restart",
        help="(optional) restart unfinished previous runs.",
        action="store_true",
    )
    run_tm.add_argument(
        "--cleanup",
        help="whether to keep temporary output files for debugging.",
        action=argparse.BooleanOptionalAction,
        default=True,
    )
    run_tm.set_defaults(func=_tm)

    # batch
    batch_descr = "Create multiline parameters from list of tomo-nums."
    batch = subparsers.add_parser(
        "batch_params",
        description=batch_descr,
        help="create batched parameters from tomo-list"
    )
    batch.add_argument(
        "-t",
        "--template",
        help="path to templated input parameters star-file. " + \
             "The parameter 'tomo_name' must contain one or more unique " + \
             "replacement tokens that can be found by the regex '\$[xX]+' " + \
             ",i.e., a sequence of 'x' or 'X' preceded by '$'. The value " + \
             "for 'tomo_num' will be overwritten accordingly in every " + \
             "line and must not be templated explicitly.",
        required=True,
    )
    batch.add_argument(
        "-b",
        "--batch",
        help="path to input tomo-list. Must be a file with a sequence of " + \
             "tomogram numbers that can be read with numpy.loadtxt.",
        required=True,
    )
    batch.add_argument(
        "-o",
        "--output",
        help="(optional) output file; default: <template-name>.batch.<suffix>",
    )
    batch.set_defaults(func=_batch_params)

    # show config
    config_descr = "Create template parameter file with defaults."
    config = subparsers.add_parser(
        "config",
        description=config_descr,
        help="create template parameter file."
    )
    config.add_argument(
        "-o",
        "--output",
        help="path to output parameters star-file.",
        required=True,
    )
    config.add_argument(
        "-n",
        "--no-defaults",
        help="Write subset of parameters without defaults. Note: " \
             "Not all parameters are necessary for all subcommands.",
        action="store_true",
    )
    config.set_defaults(func=_config)

    # generate_angles
    angles_descr = "Generate angles from cryocat.geom."
    angles = subparsers.add_parser(
        "generate_angles",
        description=angles_descr,
        help="generate angles from cryocat.geom.",
    )
    angles.add_argument(
        "--angincr",
        help='out-of-plane angular increment in degrees. Defines how ' + \
             'dense the cone will be sampled. In "zxz" Euler convention ' + \
             'influences the theta (x) and psi (z2) angles.',
        required=True,
        type=float,
    )
    angles.add_argument(
        "--angiter",
        help='(optional) out-of-plane angular iterations in degrees. ' + \
             'Defines the total cone angle to be sampled as ' + \
             'angincr*angiter*2 (the maximum should be 180 degrees for the ' + \
             'full sphere). In "zxz" Euler convention influences the theta ' + \
             '(x) and psi (z2) angles. If not specified, it will be set so ' + \
             'the full sphere is sampled.',
        type=float,
    )
    angles.add_argument(
        "--phi-angincr",
        help='(optional) in-plane angular increments in degrees. Defines ' + \
             'how densely the in-plane rotation will besampled. In "zxz" ' + \
             'Euler convention influences the phi (z1) angle. If not ' + \
             'specified it will be set with angincr parameter.',
        type=float,
    )
    angles.add_argument(
        "--phi-angiter",
        help='(optional) in-plane angular iterations. Defines the total ' + \
             'in-plane angle to be sampled as phi_angincr*phi_angiter*2 ' + \
             '(the maximum should be 180 degrees for the full circle). ' + \
             'In "zxz" Euler convention influences the phi (z1) angle. If ' + \
             'not specified, it will be set so the circle is sampled.',
        type=float,
    )
    angles.add_argument(
        "--symmetry",
        help='symmetry',
        default="C1",
    )
    angles.add_argument(
        "--order",
        help='(optional) Euler-angles spec; "zxz" or "zzx"',
        default="zxz",
    )
    angles.set_defaults(func=_angles)



    
    args = parser.parse_args()
    args.func(args)
