"""Phase randomization."""
import numpy as np

def init_phase_randomization(tmpl, tmpl_bpf, params, seed=None):
    """Initialize phase randomization."""

    if not params["noise_corr"]:
        return None

    pt_idx   = np.nonzero(tmpl_bpf)

    ft    = np.fft.fftn(tmpl)
    amp   = np.abs(ft[pt_idx])
    phase = np.exp(1j * np.angle(ft[pt_idx]))

    rng = np.random.default_rng(seed)

    r_idx    = rng.permutation(pt_idx[0].size)
    r_pt_idx = tuple(j[r_idx] for j in pt_idx)

    ftTmpl = np.zeros(tmpl.shape, dtype='complex')
    ftTmpl[r_pt_idx] = amp * phase
    pr_tmpl = np.real(np.fft.ifftn(ftTmpl))

    return pr_tmpl
