import pathlib 
import ast
import io

import numpy as np
import starfile
import pytest

from gapstop.config import read_params, _params_fields
from gapstop.config import read_wedgelist, _wedgelist_fields
from gapstop.config import read_angles, write_config, _defaults
from gapstop.config import gen_angles

_test_params = """
data_stopgap_tm_parameters

loop_
_rootdir
_tomo_name
_tomo_num
_tomo_mask_name
_wedgelist_name
_tmpl_name
_mask_name
_symmetry
_anglist_name
_smap_name
_omap_name
_tmap_name
_lp_rad
_lp_sigma
_hp_rad
_hp_sigma
_binning
_calc_exp
_calc_ctf
_apply_laplacian
_noise_corr
  {root}  tomo.mrc  100  tomo_mask.mrc  wedge_list.star  tmpl.em  mask.em  c1  anglist.csv  scores  angles  noise_corr  6.0  3.0  1.0  2.0  1  True  True  False  1.0
  {root}  tomo.mrc  100  tomo_mask.mrc  wedge_list.star  tmpl.em  mask.em  c1  anglist.csv  scores  angles  noise_corr  6.0  3.0  1.0  2.0  1  True  True  False  1.0
"""

_ref_params = "{{"\
"'rootdir':         '{root}',"\
"'outputdir':       '{root}/outputs',"\
"'vol_ext':         '.mrc',"\
"'tomo_name':       '{root}/tomo.mrc',"\
"'tomo_num':        100,"\
"'tomo_mask_name':  '{root}/tomo_mask.mrc',"\
"'wedgelist_name':  '{root}/wedge_list.star',"\
"'tmpl_name':       '{root}/tmpl.em',"\
"'mask_name':       '{root}/mask.em',"\
"'symmetry':        'c1',"\
"'anglist_name':    '{root}/anglist.csv',"\
"'smap_name':       'scores',"\
"'omap_name':       'angles',"\
"'tmap_name':       'noise_corr',"\
"'lp_rad':          6.0,"\
"'lp_sigma':        3.0,"\
"'hp_rad':          1.0,"\
"'hp_sigma':        2.0,"\
"'binning':         1,"\
"'calc_exp':        True,"\
"'calc_ctf':        True,"\
"'apply_laplacian': False,"\
"'noise_corr':      1.0,"\
"'fourier_crop':    True,"\
"'scoring_fcn':     'flcf',"\
"'write_raw':       False,"\
"'tiling':          'legacy_fix',"\
"}}"

_test_angles="""
45,0,90
37,4,20
"""

_test_wedgelist = """
data_stopgap_wedgelist

loop_
_tomo_num
_pixelsize
_tomo_x
_tomo_y
_tomo_z
_z_shift
_tilt_angle
_defocus
_exposure
_voltage
_amp_contrast
_cs
  100     1.0    10    20    30     0.0     -89.0100       3.3002     215.0108     300.0000       0.0700       2.7000
  100     1.0    10    20    30     0.0     -87.0100       3.3645     212.4548     300.0000       0.0700       2.7000
  100     1.0    10    20    30     0.0     -84.0100       3.3990     201.7838     300.0000       0.0700       2.7000
  100     1.0    10    20    30     0.0     -81.0100       3.4729     198.2278     300.0000       0.0700       2.7000
  111     1.0    10    20    30     0.0     -89.0100       3.3002     215.0108     300.0000       0.0700       2.7000
  111     1.0    10    20    30     0.0     -87.0100       3.3645     212.4548     300.0000       0.0700       2.7000
  111     1.0    10    20    30     0.0     -84.0100       3.3990     201.7838     300.0000       0.0700       2.7000
"""

_ref_angles_gen = """0.00000e+00,0.00000e+00,0.00000e+00
9.00000e+01,0.00000e+00,0.00000e+00
1.80000e+02,0.00000e+00,0.00000e+00
2.70000e+02,0.00000e+00,0.00000e+00
0.00000e+00,4.81897e+01,-1.32492e+02
9.00000e+01,4.81897e+01,-1.32492e+02
1.80000e+02,4.81897e+01,-1.32492e+02
2.70000e+02,4.81897e+01,-1.32492e+02
0.00000e+00,7.05288e+01,5.01553e+00
9.00000e+01,7.05288e+01,5.01553e+00
1.80000e+02,7.05288e+01,5.01553e+00
2.70000e+02,7.05288e+01,5.01553e+00
0.00000e+00,9.00000e+01,1.42523e+02
9.00000e+01,9.00000e+01,1.42523e+02
1.80000e+02,9.00000e+01,1.42523e+02
2.70000e+02,9.00000e+01,1.42523e+02
0.00000e+00,1.09471e+02,-7.99689e+01
9.00000e+01,1.09471e+02,-7.99689e+01
1.80000e+02,1.09471e+02,-7.99689e+01
2.70000e+02,1.09471e+02,-7.99689e+01
0.00000e+00,1.31810e+02,5.75388e+01
9.00000e+01,1.31810e+02,5.75388e+01
1.80000e+02,1.31810e+02,5.75388e+01
2.70000e+02,1.31810e+02,5.75388e+01
0.00000e+00,1.80000e+02,0.00000e+00
9.00000e+01,1.80000e+02,0.00000e+00
1.80000e+02,1.80000e+02,0.00000e+00
2.70000e+02,1.80000e+02,0.00000e+00
"""

@pytest.fixture(scope='module')
def wdir(tmp_path_factory):
    """Get a working directory for this module's test."""
    return tmp_path_factory.mktemp('test-config')

def test_params(wdir):
    """Test reading of params from star-file."""

    # write test params
    params_file = wdir / "tm_params.star"
    with params_file.open(mode="w") as fp:
        fp.write(_test_params.format(root=str(wdir)))

    params = read_params(params_file)
    ref    = ast.literal_eval(_ref_params.format(root=str(wdir)))

    assert len(params) == 2
    for i, row in params.iterrows():
        assert np.all([row[k] == v for k,v in ref.items()])

def test_wedgelist(wdir):
    """Test reading of wedgelist information from star-file."""

    # write test wedgelist
    wedge_file = wdir / "wedges.star"
    with wedge_file.open(mode="w") as fp:
        fp.write(_test_wedgelist)

    wl = read_wedgelist(wedge_file)
    
    assert len(wl) == 2

    # check whether types are correctly transformed
    for r in wl.iterrows():
        for k,v in r[1].items():
            assert isinstance(v, _wedgelist_fields[k][0]) or \
                all([isinstance(vi, _wedgelist_fields[k][0]) for vi in v])

def test_angles_read(wdir):

    # write test angles
    angle_file = wdir / "anglist.csv"
    with angle_file.open(mode="w") as fp:
        fp.write(_test_angles)


    params = {
        "anglist_name": str(angle_file),
        "anglist_order": "zxz",
    }

    angles = read_angles(params)

    ref = np.array([
        [45, 0, 90],
        [37, 4, 20]
    ])

    assert (angles == ref).all()

def test_angles_generator(wdir):
    """test generation of angles from cryocat.geom."""

    with io.StringIO(_ref_angles_gen) as fp:
        ref_angles = np.loadtxt(fp, delimiter=",")

    gen_angles(100.0,180/100,100.0,180/100, "C1", "zxz", outroot=wdir)
    res = wdir / "angles_100.0_1.8_100.0_1.8.txt"
    angles = np.loadtxt(res, delimiter=",")
    assert np.all(angles == ref_angles)

    gen_angles(100.0,180/100,100.0,180/100, "C1", "zzx", outroot=wdir)
    angles = np.loadtxt(res, delimiter=",")
    assert np.all(angles == ref_angles[:,[0,2,1]])

    with pytest.raises(ValueError):
        gen_angles(100.0,180/100,100.0,180/100, "C1", "foo", outroot=wdir)
  
def test_defaults(tmp_path):
    """Test writing of defaults."""

    outfile = tmp_path / "foo.star"
    write_config(outfile)

    defaults = starfile.read(outfile)

    # check defaults
    for k,v in _defaults.items():
        assert defaults.loc[0,k] == v
