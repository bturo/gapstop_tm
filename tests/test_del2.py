import numpy as np

from gapstop.del2 import del2

def test_del2():
    """Test action of the discrete Laplace operator."""

    # zero curvature
    a = np.ones((5,5,5))
    da = del2(a)
    assert np.allclose(da, 0)
    
    # constant curvature (1d)
    a = np.arange(1,6)**2
    da = del2(a)
    assert np.allclose(da, 0.5)

    # constant curvature (2d)
    a = np.arange(1,6)**2
    a = a + a[:,None]
    da = del2(a)
    assert np.allclose(da, 1.0)

    # constant curvature (3d)
    a = np.arange(1,6)**2
    a = a + a[:,None] + a[:,None,None]
    da = del2(a)
    # also 1 due to del2's scaling with 1/4 for 1d/2d and 1/2N for 3d! 
    assert np.allclose(da, 1.0)

    # linear curvature (2d)
    a = np.arange(1,6)**3
    da = del2(a)
    assert np.allclose(np.diff(da), 1.5)

    # linear curvature (2d)
    a = np.arange(1,6)**3
    a = a + a[:,None]
    da = del2(a)
    assert np.allclose(np.diff(da), 1.5)

    # linear curvature (2d)
    a = np.arange(1,6)**3
    a = a + a[:,None] + a[:,None,None]
    da = del2(a)
    # 1 here due to del2's scaling with 1/4 for 1d/2d and 1/2N for 3d! 
    assert np.allclose(np.diff(da), 1.0)
