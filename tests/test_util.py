import filecmp

import numpy as np
import pytest
import mrcfile
import emfile

from gapstop.util import read_volume, write_volume

_vol = np.random.randn(2,3,4).astype(np.float32)

def _test_vols(wdir):
    """Put test volumes as cryo-files in wdir."""
    mrcfile.write(wdir / "foo.mrc", _vol, voxel_size=1)
    emfile.write(wdir / "foo.em", _vol)

@pytest.fixture(scope="module")
def wdir(tmp_path_factory):
    """Generate test data."""
    wdir = tmp_path_factory.mktemp('test-util')
    _test_vols(wdir)
    return wdir

def test_volume_io(wdir):
    """Test read/write_volume."""

    vol = read_volume(wdir / "foo.mrc")
    assert (vol == _vol).all()

    vol = read_volume(wdir / "foo.em")
    assert (vol == _vol).all()

    write_volume(wdir / "foo_new.mrc", _vol)
    assert filecmp.cmp(wdir / "foo.mrc", wdir / "foo_new.mrc")

    write_volume(wdir / "foo_new.em", _vol)
    assert filecmp.cmp(wdir / "foo.em", wdir / "foo_new.em")
