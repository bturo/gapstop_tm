import pathlib
import os
from collections import namedtuple

import numpy as np
import pandas as pd
import starfile

from gapstop.config import read_params
from gapstop.util import write_volume, read_volume
from gapstop.prepare_template import get_template_and_mask
from gapstop.dd import compute_tiles

import pytest

def _mock_posf(folder, vol_shape, tmpl_shape, bd, with_mask=False):
    """Mock posf data."""

    # tomogram 
    vol   = np.zeros(vol_shape, dtype=np.float32)
    mask  = np.zeros_like(vol)
    mask[bd:-bd,bd:-bd,bd:-bd] = 1

    # template
    tmpl      = np.zeros(tmpl_shape, dtype=np.float32)
    tmpl_mask = np.zeros_like(tmpl)

    # create folders
    folder = pathlib.Path(folder)  
    os.mkdir(folder / "inputs")
    os.mkdir(folder / "outputs")
    os.mkdir(folder / "temp")

    # write volumes
    tomo_name      = folder / "tomo_111.rec"
    tomo_mask_name = folder / "tomo_mask_111.rec"
    tmpl_name      = folder / "inputs/tmpl.em"
    mask_name      = folder / "inputs/mask.em"
    write_volume(tomo_name, vol)
    write_volume(tomo_mask_name, mask)
    write_volume(tmpl_name, tmpl)
    write_volume(mask_name, tmpl_mask)

    #params
    p = pd.DataFrame({
        "rootdir":         str(folder.resolve())+"/",
        "tomo_name":       str(tomo_name.resolve()),
        "tomo_num":        "111",
        "tomo_mask_name":  str(tomo_mask_name.resolve()),
        "wedgelist_name":  "inputs/wedge_list.star",
        "tmpl_name":       "inputs/tmpl.em",
        "mask_name":       "inputs/mask.em",
        "symmetry":        "c1",
        "anglist_name":    "inputs/ang_list.csv",
        "smap_name":       "scores",
        "omap_name":        "angles",
        "tmap_name":       "noise",
        "lp_rad":          34,
        "lp_sigma":        3,
        "hp_rad":          1,
        "hp_sigma":        2,
        "binning":         2,
        "calc_exp":        1,
        "calc_ctf":        1,
        "apply_laplacian": 0,
        "noise_corr":      1,
    }, index=[0])
    if not with_mask:
        p = p.drop(labels=["tomo_mask_name"], axis=1)
    starfile.write({"stopap_tm_parameters": p}, folder / "tm_param.star")

    s = "vol_ext=.em\nfourier_crop=false\nwrite_raw=true"
    with open(folder / "tm_settings.txt", "w") as fp:
        fp.write(s)

    angles = np.array([[10,20,30],[0,40,50]])
    np.savetxt(folder / "inputs/ang_list.csv", angles, fmt="%d", delimiter=",")

    wedge = {"stopgap_wedgelist": pd.DataFrame({
        "tomo_num":     [111]*3,
        "pixelsize":    [1.2]*3,
        "tomo_x":       [vol_shape[2]]*3,
        "tomo_y":       [vol_shape[1]]*3,
        "tomo_z":       [vol_shape[0]]*3,
        "z_shift":      [-40.00]*3,
        "tilt_angle":   [-51.2]*3,
        "defocus":      [1.234]*3,
        "exposure":     [150.234]*3,
        "voltage":      [300.000]*3,
        "amp_contrast": [0.0700]*3,
        "cs":           [2.7000]*3,
    })}
    starfile.write(wedge, folder / "inputs/wedge_list.star", overwrite=True)

TestParam = namedtuple(
    "TestParam", 
    [
        "vol_shape",
        "tmpl_shape",
        "masked_boundary_size",
        "with_mask",
    ]
)
 
@pytest.fixture(
    scope="module",
    params=[
        TestParam((20,51,72), (4,4,4), 2, True),
        TestParam((20,71,51), (4,4,4), 2, True),
        TestParam((51,71,20), (4,4,4), 2, True),
        TestParam((20,51,72), (4,4,4), 2, False),
        TestParam((20,51,72), (5,5,5), 2, False),
        TestParam((20,51,72), (5,5,5), 2, True),
    ],
)
def mock_dir(request, tmp_path_factory):
    p = request.param
    mock_dir = tmp_path_factory.mktemp('mock')
    _mock_posf(
        mock_dir,
        p.vol_shape,
        p.tmpl_shape,
        p.masked_boundary_size,
        p.with_mask,
    )
    return mock_dir

@pytest.fixture(scope="function", params=[7,12,16])
def ntiles(request):
    return request.param

def test_tiling(mock_dir, ntiles):
    """Test tiling.

    By cycling through extraction-pasting-cropping-insertion,
    a tomogram must be exactly reproduced in the masked region.
    """
        
    # get tiling
    conf = read_params(mock_dir / "tm_param.star").iloc[0]
    tomo = read_volume(conf["tomo_name"])
    tmpl, mask = get_template_and_mask(conf)
    conf["tiling"] = "new"
    c, tilesize = compute_tiles(conf, ntiles, tmpl)

    data = np.arange(1,np.prod(tomo.shape)+1, dtype=np.float32)
    data = data.reshape(tomo.shape)
    out  = np.zeros_like(data)

    for t in range(ntiles):
        tile = np.ones(tilesize) * -1

        # extract from data and paste into local tile
        paste = tuple([slice(ts, te) for ts,te in zip(c["ts"][t], c["te"][t])])
        extract = tuple([slice(es, ee) for es,ee in zip(c["es"][t], c["ee"][t])])
        tile[paste] = data[extract] 

        # crop from tile and insert into new tomogram
        crop   = tuple([slice(cs, ce) for cs,ce in zip(c["cs"][t], c["ce"][t])])
        insert = tuple([slice(bs, be) for bs,be in zip(c["bs"][t], c["be"][t])])
        out[insert] = tile[crop]

    tomo_mask_name = conf.get("tomo_mask_name")
    if not tomo_mask_name is None:
        tomo_mask = read_volume(tomo_mask_name)
        data *= tomo_mask
        out  *= tomo_mask
    assert np.all(data == out)
