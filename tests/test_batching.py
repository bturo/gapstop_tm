import io

import starfile

from gapstop.util import batch_params

template = """data_stopgap_tm_parameters

loop_
_tomo_name
_tomo_num
_blub
 /foo/bar/baz_$XXX.rec  126  data
"""

batch = """123
456
7
89
"""

ref = """data_stopgap_tm_parameters

loop_
_tomo_name
_tomo_num
_blub
 /foo/bar/baz_123.rec  123  data
 /foo/bar/baz_456.rec  456  data
 /foo/bar/baz_007.rec  7  data
 /foo/bar/baz_089.rec  89  data
"""

def test_batching(tmp_path):
    """Test creation of batched parameters from template."""

    tmpl_file  = tmp_path / "template"
    batch_file = tmp_path / "batch"
    ref_file   = tmp_path / "ref.star"
    res_file   = tmp_path / "res.star"

    with open(tmpl_file, "w") as fp:
        fp.write(template)

    with open(batch_file, "w") as fp:
        fp.write(batch)

    with open(ref_file, "w") as fp:
        fp.write(ref)

    batch_params(batch_file, tmpl_file, res_file)
    assert res_file.exists()

    res = starfile.read(res_file)

    _ref = starfile.read(tmp_path / "ref.star")
    assert (_ref == res).all(None)
    
    
